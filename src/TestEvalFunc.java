import static org.junit.Assert.*;

import org.junit.Test;


public class TestEvalFunc {

	@Test
	public void testCornerEvaluation1() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("WOOOOOOOOXXXXXXOOXXXXXOOOXOOXXOOXXOXOOOXXXXOOOOXXXEEEEEEEEEEEEEEE");
		System.out.println("Corner eval 1");
		pos.illustrate();
		int value = eval.cornerValue(pos);
		assertEquals(100, value);
	}
	
	@Test
	public void testCornerEvaluation2() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("BEEEEEEEEEOOEEEEEEEOXXXEEEEEOOEEEEEEXOOEEEEEEEEEEEEEEEEEEEEEEEEEE");
		System.out.println("Corner eval 2");
		pos.illustrate();
		int value = eval.cornerValue(pos);
		assertEquals(-100, value);
	}
	
	@Test
	public void testCornerEvaluation3() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("BEXEEEEEEEOOEEEEEEEOXXXEEEEEOOEEEEEEXOOEEEEEEEEEEEEEEEEEEEEEEEEEE");
		System.out.println("Corner eval 3");
		pos.illustrate();
		int value = eval.cornerValue(pos);
		assertEquals(-14, value);
	}
	
	@Test
	public void testCornerEvaluation4() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("WOOOXEEEEEEXOEEEEEEEXXXXXEEEXXEEEEEEXOEEEEEXOEEEEEEEEEEEEEEEEEEEE");
		System.out.println("Corner eval 4");
		pos.illustrate();
		int value = eval.cornerValue(pos);
		assertEquals(100, value);
	}
	
	
	
	@Test
	public void testStabilityEvalutation1() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("WOOOXEEEEEEXOEEEEEEEXXXXXEEEXXEEEEEEXOEEEEEXOEEEEEEEEEEEEEEEEEEEE");
		System.out.println("Stability eval 1");
		pos.illustrate();
		int value = eval.stability(pos);
		assertEquals(57, value);
	}
	
	@Test
	public void testCoinParityEvaluation1() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("WEXXXXXEEEEXXXEEEEXXXXEEEEEXOOOXEEEOXXXEEEEEEXEEEEEEEEEEEEEEEEEEE");
		System.out.println("Coin parity eval 1");
		pos.illustrate();
		int value = eval.coinParity(pos);
		assertEquals(-63, value);
	}
	
	@Test
	public void testMobilityEvaluation1() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("WEEXXEEEEXEXXXXEEXXOOXXEEXXXXXEEEEEEXOEEEEEXEEEEEEXEEEEEEEEEEEEEE");
		System.out.println("Mobility eval 1");
		pos.illustrate();
		int value = eval.mobility(pos);
		assertEquals(38, value);
	}
	
	@Test
	public void testCornerEvaluation5() {
		HeuristicEvaluator eval = new HeuristicEvaluator();
		OthelloPosition pos = new OthelloPosition("BOOOOOOOOOOOXXXOOOOXXXXXOOOOOOXXOXOOOOXOOXOOXXOXOEOXXEXXXEXEEEEEE");
		System.out.println("Corner eval 5");
		pos.illustrate();
		int value = eval.cornerValue(pos);
		assertEquals(73, value);
	}
	
	
}
