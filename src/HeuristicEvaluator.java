import java.util.ArrayList;


public class HeuristicEvaluator implements OthelloEvaluator {
	
	private int[][] cornerWeights = {{8,-3,0,0,0,0,-3,8},
								  {-3,-4,0,0,0,0,-4,-3},
								  {0,0,0,0,0,0,0,0},
								  {0,0,0,0,0,0,0,0},
								  {0,0,0,0,0,0,0,0},
								  {0,0,0,0,0,0,0,0},
								  {-3,-4,0,0,0,0,-4,-3},
								  {8,-3,0,0,0,0,-3,8}};

	@Override
	public int evaluate(OthelloPosition position) {
		
		return (cornerValue(position) + stability(position) + mobility(position) + coinParity(position));
	}
	
	public int coinParity(OthelloPosition position) {
		int maxPlayerCoins = 0;
		int minPlayerCoins = 0;
		for (int i=1; i<=OthelloPosition.BOARD_SIZE; i++) {
			for (int j=1; j<=OthelloPosition.BOARD_SIZE; j++) {
				if (position.board[i][j] == 'B')
					minPlayerCoins++;
				else if (position.board[i][j] == 'W')
					maxPlayerCoins++;
			}
		}
		
		return 100*(maxPlayerCoins-minPlayerCoins)/(maxPlayerCoins+minPlayerCoins);
	}
	
	public int mobility(OthelloPosition position) {
		int maxPlayerMoves = 0;
		int minPlayerMoves = 0;
		for (int i=1; i<=OthelloPosition.BOARD_SIZE; i++) {
			for (int j=1; j<=OthelloPosition.BOARD_SIZE; j++) {
				if (position.isValidMove(i, j, 'B'))
					minPlayerMoves++;
				if (position.isValidMove(i, j, 'W'))
					maxPlayerMoves++;
			}
		}
		int value = 0;
		if (maxPlayerMoves + minPlayerMoves != 0) {
			value = 100*(maxPlayerMoves-minPlayerMoves) / (maxPlayerMoves+minPlayerMoves);
		}
		return value;
	}
	
	public int stability(OthelloPosition position) {
		int maxPlayerMoves = 0;
		int minPlayerMoves = 0;
		int stabilityValue = 0;
		
		for (int i=1; i<=OthelloPosition.BOARD_SIZE; i++) {
			for (int j=1; j<=OthelloPosition.BOARD_SIZE; j++) {
				if (position.board[i][j] == 'B') {
					stabilityValue = getStabilityValue(position, new int[] {i,j}, 'W');
					if (stabilityValue < 0) {
						maxPlayerMoves -= stabilityValue;
					} else {
						minPlayerMoves += stabilityValue;
					}
				} else if (position.board[i][j] == 'W') {
					stabilityValue = getStabilityValue(position, new int[] {i,j}, 'B');
					if (stabilityValue < 0) {
						minPlayerMoves -= stabilityValue;
					} else {
						maxPlayerMoves += stabilityValue;
					}
				}
			}
		}
		int value = 0;
		if (maxPlayerMoves + minPlayerMoves != 0) {
			value = 100*(maxPlayerMoves-minPlayerMoves) / (maxPlayerMoves+minPlayerMoves);
		}
		return value;
	}
	
	private int getStabilityValue(OthelloPosition board, int[] pos, char opponent) {
		ArrayList<int[]> adjacentEmptySquares = board.getAdjacentEmptySquares(pos[0], pos[1]);
		boolean unstable = false;
		boolean semistable = false;
		for (int[] emptyPos : adjacentEmptySquares) {
			int di = emptyPos[0]-pos[0]; int dj = emptyPos[1]-pos[1];
			
			boolean foundEmpty = false; boolean foundOpponent = false;
			int k = 1;
			while (pos[0]+di*k >= 1 && pos[0]+di*k <= OthelloPosition.BOARD_SIZE &&
					pos[1]+dj*k >= 1 && pos[1]+dj*k <= OthelloPosition.BOARD_SIZE) {
				
				if (board.board[pos[0]+di*k][pos[1]+dj*k] == opponent)
					foundOpponent = true;
				if (board.board[pos[0]+di*k][pos[1]+dj*k] == 'E')
					foundEmpty = true;
				k++;
			}
			int l = -1;
			while (pos[0]+di*l >= 1 && pos[0]+di*l <= OthelloPosition.BOARD_SIZE &&
					pos[1]+dj*l >= 1 && pos[1]+dj*l <= OthelloPosition.BOARD_SIZE) {
				
				if (board.board[pos[0]+di*l][pos[1]+dj*l] == opponent) {
					if (foundEmpty)
						unstable = true;
				}
					
				if (board.board[pos[0]+di*l][pos[1]+dj*l] == 'E') {
					if (foundOpponent)
						unstable = true;
					if (foundEmpty)
						semistable = true;
				}
					
				l--;
			}
		}
		if (unstable) return -1;
		if (semistable) return 0;
		return 1;
	}
	
	public int cornerValue(OthelloPosition position) {
		int maxPlayerValue = 0;
		int minPlayerValue = 0;
		
		for (int i=1; i<=OthelloPosition.BOARD_SIZE; i++) {
			for (int j=1; j<=OthelloPosition.BOARD_SIZE; j++) {
				if (position.board[i][j] == 'W') {
					if (cornerWeights[i-1][j-1] < 0) {
						if (!hasAdjacentCorner(position, new int[] {i,j}, 'W'))
							minPlayerValue -= cornerWeights[i-1][j-1];
					} else {
						maxPlayerValue += cornerWeights[i-1][j-1];
					}
				}
				else if (position.board[i][j] == 'B') {
					if (cornerWeights[i-1][j-1] < 0) {
						if (!hasAdjacentCorner(position, new int[] {i,j}, 'W'));
							maxPlayerValue -= cornerWeights[i-1][j-1];
					} else {
						minPlayerValue += cornerWeights[i-1][j-1];
					}
				}
			}
		} 
		int value = 0;
		if (maxPlayerValue+minPlayerValue != 0)
			value = 100*(maxPlayerValue-minPlayerValue)/(maxPlayerValue+minPlayerValue);
		return value;
	}
	
	private boolean hasAdjacentCorner(OthelloPosition board, int[] pos, char player) {
		if (pos[0] == 1 && pos[1] == 2 || pos[0] == 2 && pos[1] == 1 || pos[0] == 2 && pos[1] == 2) {
			if (board.board[1][1] == player) return true;
		}
		if (pos[0] == 1 && pos[1] == 7 || pos[0] == 2 && pos[1] == 7 || pos[0] == 2 && pos[1] == 8) {
			if (board.board[1][8] == player) return true;
		}
		if (pos[0] == 7 && pos[1] == 2 || pos[0] == 7 && pos[1] == 2 || pos[0] == 8 && pos[1] == 2) {
			if (board.board[8][1] == player) return true;
		}
		if (pos[0] == 7 && pos[1] == 7 || pos[0] == 7 && pos[1] == 8 || pos[0] == 8 && pos[1] == 7) {
			if (board.board[8][8] == player) return true;
		}
			
		return false;
	}

}


/* Code for not giving negative values adjacent to corner owned */

//char corner = 'E';
//// Left upper corner
//if (position.board[1][1] == 'W') {
//	corner = 'W';
//	maxPlayerValue += cornerWeights[0][0];
//}	
//if (position.board[1][1] == 'B') {
//	corner = 'B';
//	minPlayerValue += cornerWeights[0][0];
//}
//
//for (int i=1; i<=2; i++) {
//	for (int j=1; j<=2; j++) {
//		if (position.board[i][j] == 'W' && corner != 'W')
//			maxPlayerValue += cornerWeights[i-1][j-1];
//		else if (position.board[i][j] == 'B' && corner != 'B')
//			minPlayerValue += cornerWeights[i-1][j-1];	
//	}
//}

//corner = 'E';
//// Right upper corner
//if (position.board[1][8] == 'W') {
//	corner = 'W';
//	maxPlayerValue += cornerWeights[0][0];
//}
//if (position.board[1][8] == 'B') {
//	corner = 'B';
//	minPlayerValue += cornerWeights[0][0];
//}
//for (int i=1; i<=2; i++) {
//	for (int j=OthelloPosition.BOARD_SIZE-1; j<=OthelloPosition.BOARD_SIZE; j++) {
//		if (position.board[i][j] == 'W' && corner != 'W')
//			maxPlayerValue += cornerWeights[i-1][OthelloPosition.BOARD_SIZE-j];
//		else if (position.board[i][j] == 'B' && corner != 'B')
//			minPlayerValue += cornerWeights[i-1][OthelloPosition.BOARD_SIZE-j];	
//	}
//}
//corner = 'E';
//// Left lower corner
//if (position.board[8][1] == 'W') {
//	corner = 'W';
//	maxPlayerValue += cornerWeights[0][0];
//}
//if (position.board[8][1] == 'B') {
//	corner = 'B';
//	minPlayerValue += cornerWeights[0][0];
//}
//for (int i=OthelloPosition.BOARD_SIZE-1; i<=OthelloPosition.BOARD_SIZE; i++) {
//	for (int j=1; j<=2; j++) {
//		if (position.board[i][j] == 'W' && corner != 'W')
//			maxPlayerValue += cornerWeights[OthelloPosition.BOARD_SIZE-i][j-1];
//		else if (position.board[i][j] == 'B' && corner != 'B')
//			minPlayerValue += cornerWeights[OthelloPosition.BOARD_SIZE-i][j-1];	
//	}
//}
//corner = 'E';
//// Right lower corner
//if (position.board[8][8] == 'W') {
//	corner = 'W';
//	maxPlayerValue += cornerWeights[0][0];
//}
//	
//if (position.board[8][8] == 'B') {
//	corner = 'B';
//	minPlayerValue += cornerWeights[0][0];
//}
//	
//for (int i=OthelloPosition.BOARD_SIZE-1; i<=OthelloPosition.BOARD_SIZE; i++) {
//	for (int j=OthelloPosition.BOARD_SIZE-1; j<=OthelloPosition.BOARD_SIZE; j++) {
//		if (position.board[i][j] == 'W' && corner != 'W')
//			maxPlayerValue += cornerWeights[OthelloPosition.BOARD_SIZE-i][OthelloPosition.BOARD_SIZE-j];
//		else if (position.board[i][j] == 'B' && corner != 'B')
//			minPlayerValue += cornerWeights[OthelloPosition.BOARD_SIZE-i][OthelloPosition.BOARD_SIZE-j];	
//	}

