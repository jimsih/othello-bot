
public class Othello {

	public static void main(String[] args) throws IllegalMoveException {
		if (args.length < 2) {
			System.out.println(args.length);
			System.out.println("Usage: Othello board depth");
			System.exit(-1);
		}
		
		if (args[0].length() != 65) {
			System.out.println("Board must be a string of 65 characters");
			System.exit(-1);
		}
		
		int depth = 0;
		try {
			depth = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Time limit must be an integer");
			System.exit(-1);
		}
		
		if (depth < 1) {
			System.out.println("Time limit must be a positive integer");
			System.exit(-1);
		}
		
		OthelloPosition pos = new OthelloPosition(args[0]);
		OthelloAlgorithm alphabeta = new AlphaBeta();
		alphabeta.setEvaluator(new HeuristicEvaluator());
		alphabeta.setSearchDepth(depth);
		
		alphabeta.evaluate(pos).print();
	}

}
