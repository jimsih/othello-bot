import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;


public class AlphaBeta implements OthelloAlgorithm {
	
	private OthelloEvaluator evaluator;
	private int time;
	private boolean cancel;
	private Timer timer;
	
	public AlphaBeta() {
		evaluator = new StaticEvaluator();
		time = 5;
		cancel = false;
	}

	@Override
	public void setEvaluator(OthelloEvaluator evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public OthelloAction evaluate(OthelloPosition position) {
		OthelloAction bestMove = new OthelloAction("pass");
		LinkedList<OthelloAction> children = position.getMoves();
		if (children.size() == 0)
			return bestMove;
		
		setTimer();
		int value = position.playerToMove ? Integer.MIN_VALUE : Integer.MAX_VALUE;
		int depth = 1;
		/* Iterative deepening */
		while (!cancel) {
			for (OthelloAction child : children) {
				if (cancel) break;
				try {	/* White player starts, Max */
					if (position.playerToMove) {
						child.setValue(minValue(position.makeMove(child), depth, Integer.MIN_VALUE, Integer.MAX_VALUE));
						if (child.value > value && child.value != Integer.MAX_VALUE) {
							value = child.value;
							bestMove = child;
						}
					} else { /* Black player starts, Min */
						child.setValue(maxValue(position.makeMove(child), depth, Integer.MIN_VALUE, Integer.MAX_VALUE));
						if (child.value < value && child.value != Integer.MIN_VALUE) {
							value = child.value;
							bestMove = child;
						}
					}
				} catch (IllegalMoveException e) {
					e.printStackTrace();
				}
			}
			
			/* Break if all leaves are found */
			//System.out.println(depth);
			depth++;
			
		}
		timer.cancel();
		return bestMove;
	}
	
	private int maxValue(OthelloPosition node, int depth, int a, int b) throws IllegalMoveException {
		LinkedList<OthelloAction> children = node.getMoves();
		if (depth == 0 || children.isEmpty()) {
			int eval = evaluator.evaluate(node);
			return eval;
		}
		int value = Integer.MIN_VALUE;
		for (OthelloAction child : children) {
			if (cancel) break;
			value = Math.max(value, minValue(node.makeMove(child), depth-1, a, b));
			if (value >= b) { // Beta-cutoff
				return value;
			}
			a = Math.max(a, value);
		}
		return value;
	}
	
	private int minValue(OthelloPosition node, int depth, int a, int b) throws IllegalMoveException {
		LinkedList<OthelloAction> children = node.getMoves();
		if (depth == 0 || children.isEmpty()) {
			return evaluator.evaluate(node);
		}
		int value = Integer.MAX_VALUE;
		for (OthelloAction child : children) {
			if (cancel) break;
			value = Math.min(value, maxValue(node.makeMove(child), depth-1, a, b));
			if (value <= a) { // Alpha-cutoff
				return value;
			}
			b = Math.min(b, value);
		}
		return value;
	}

	@Override
	public void setSearchDepth(int time) {
		this.time = time;
	}
	
	private void setTimer() {
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				cancel = true;
			}
		}, (long) ((time-0.5)*1000));
	}

}
