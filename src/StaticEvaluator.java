
public class StaticEvaluator implements OthelloEvaluator {

	static private int[][] positionWeights = {{8,-3,2,2,2,2,-3,8},
											  {-3,-4,-1,-1,-1,-1,-4,-3},
											  {2,-1,1,0,0,1,-1,2},
											  {2,-1,0,1,1,0,-1,2},
											  {2,-1,0,1,1,0,-1,2},
											  {2,-1,1,0,0,1,-1,2},
											  {-3,-4,-1,-1,-1,-1,-4,-3},
											  {8,-3,2,2,2,2,-3,8}};
	@Override
	public int evaluate(OthelloPosition position) {
		int value = 0;
		for (int i=1; i<=OthelloPosition.BOARD_SIZE; i++) {
			for (int j=1; j<=OthelloPosition.BOARD_SIZE; j++) {
				if (position.board[i][j] == 'W')
					value += positionWeights[i-1][j-1];
				else if (position.board[i][j] == 'B')
					value -= positionWeights[i-1][j-1];	
			}
		}
		//position.illustrate();
		//System.out.println("Value: " + value);
		return value;
	}

}
