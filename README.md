Othello bot using alpha beta search

The program takes two arguments, a description of the position and a time limit. The first argument is an ascii string of length 65. The first character is W or B, and indicates which player is to move. The remaining characters should be E (for empty) O (for white markers), or X (for black markers). These 64 charachters describe the board, with the first character referring to the upper left hand corner, the second character referring to the second square from the left on the uppermost row, and so on. For example, the 10th character describes the second square from the left on the second row from the top.

The second parameter gives the number of seconds the bot is allowed to compute.

The program outputs on the format (4,7), indicating that the move suggested is to place a marker on the fourth row from the top and the seventh column from the left. If the player who has the move has no legal move, the program will instead output pass.